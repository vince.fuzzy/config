# ~/.tmux.conf

# this is for macos pbcopy clipboard. to make it work for linux, install xclip and find where it says pbcopy
# in this document, and use the following instead:
# bind-key -T copy-mode-vi Enter send -X copy-pipe-and-cancel "xclip -selection clipboard"
# you can just give this entire config file to gpt and have it convert pbcopy to xclip 

# Home-row prefix of C-k is more valuable than C-[hjkl] navigation because of "bind-key -r"
unbind-key C-k
set-option -g prefix C-k
bind-key C-k send-prefix

set -g base-index 1

#set-option -g default-terminal screen-256color
set-option -g default-terminal xterm-256color

setw -g mouse on

setw -g mode-keys vi
bind-key : command-prompt
bind-key r refresh-client
#bind-key L clear-history

# next-window and previous-window are "n" and "p" by default
bind-key enter next-layout

# vim-like split.  "Split" and "Vertical Split"
bind-key s split-window -v
bind-key v split-window -h

# The default is bound to ask for confirmation. These dont ask for confirmation before kill
bind-key & kill-window
bind-key x kill-pane

# Intuitive split characters
bind-key | split-window -h
bind-key _ split-window -v

bind-key -r h select-pane -L
bind-key -r j select-pane -D
bind-key -r k select-pane -U
bind-key -r l select-pane -R

# smart pane switching with awareness of vim splits
bind -n C-h run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-h) || tmux select-pane -L"
bind -n C-j run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-j) || tmux select-pane -D"
bind -n C-k run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-k) || tmux select-pane -U"
bind -n C-l run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-l) || tmux select-pane -R"
bind -n 'C-\' run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys 'C-\\') || tmux select-pane -l"
bind C-l send-keys 'C-l'

bind-key -r J resize-pane -D 2
bind-key -r K resize-pane -U 2
bind-key -r H resize-pane -L 5
bind-key -r L resize-pane -R 5

bind-key + select-layout main-horizontal
bind-key = select-layout main-vertical

#set-window-option -g other-pane-height 25
#set-window-option -g other-pane-width 80
#set-window-option -g display-panes-time 1500

bind-key a last-pane
bind-key q display-panes
bind-key c new-window
bind-key t next-window
bind-key T previous-window

bind-key [ copy-mode
bind-key ] paste-buffer

# Update default binding of `Enter` to also use copy-pipe
unbind -T copy-mode-vi Enter
bind-key -T copy-mode-vi Enter send -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"

# Set window notifications
setw -g monitor-activity on
set -g visual-activity on

# Enable native Mac OS X copy/paste
set-option -g default-command "/bin/bash -c 'which reattach-to-user-namespace >/dev/null && exec reattach-to-user-namespace $SHELL -l || exec $SHELL -l'"

# Time allowed to press a sequence of keys without re-sending the prefix
set-option -g repeat-time 500

# Messages (including errors) shown for x milliseconds. Default was 700ms
set-option -g display-time 3000

set -g status-style "bg=#282829,fg=#585858"
set-window-option -g window-status-style fg='#585858',bg=default
#set-window-option -g window-status-current-style fg='#ddbb00',bg=default # fg highlight!
#set-window-option -g window-status-current-style fg='#001020',bg=#d07000
#set-window-option -g window-status-current-style fg='#000000',bg=#2050a0 #blue
set-window-option -g window-status-current-style fg='#000000',bg=#505050

set -g status-right ""

#set -ag window-style 'bg=#000000'
set -g window-active-style 'bg=#0b0b0b'

set -g pane-border-style "bg=default,fg=#606060"
set -g pane-active-border-style "bg=default,fg=#808070"

set -g window-status-separator ""
set -g window-status-format "  #I #W  "
set -g window-status-current-format "  #I #W  "